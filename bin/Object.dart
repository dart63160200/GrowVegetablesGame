import 'Garden.dart';
import 'package:chalkdart/chalk.dart';
class Obj {
  late int row;
  late int col;
  late var name;

  Obj(String name, int row, int col) {
    // constructor
    this.name = name;
    this.row = row;
    this.col = col;
  }
  get getRow {
    // get เเถว
    return this.row;
  }

  get getCol {
    // get คอลัม
    return this.col;
  }

  get getName {
    // get ชื่อ
    return this.name;
  }



  bool isOn(int x, int y) {
    return this.row == x && this.col == y;
  }

}

class Me extends Obj {
  // ME สืบทอด Object
  late int money; // เงิน
  late Garden garden;

  // constructor
  Me(super.name, super.row, super.col, this.money, this.garden);

  get getMoney {
    // get เงิน
    return this.money;
  }

  bool walk(var direction) {
    // การเดิน
    switch (direction) {
      case 'N': // บน
      case 'n':
        if (walkN()) {
          return false;
        }
        break;
      case 'S':
      case 's':
        if (walkS()) {
          return false;
        }
        break;
      case 'E':
      case 'e':
        if (walkE()) return false;

        break;
      case 'W':
      case 'w':
        if (walkW()) return false;

        break;

      default:
        return false;
    }
    return true;
  }

  bool canWalk(int x, int y) {    //สามารถเดินได้
    return garden.inGarden(x, y);
  }

  bool walkW() {
    // เดินทางซ้าย

    if (canWalk(row - 1, col)) {
      row = row - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    //เดินลงล่าง
    if (canWalk(row, col + 1)) {
      col = col + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    //เดินทางขวา
    if (canWalk(row + 1, col)) {
      row = row + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    // เดินขึ้นบน
    
    if (canWalk(row, col - 1)) {
      col = col - 1;
    } else {
      return true;
    }
    return false;
  }

  bool CheckVeg(int x, int y){
    
    return false;
  }

  void setMoney(int pay) {
    this.money = pay;
  }

}

/* --------------------------------------- */
class Vegetable extends Obj {
  late int price;
  late int amount;

// constructor
  Vegetable(super.name, super.row, super.col, this.price);

  get getPrice {
    return this.price = price;
  }

  get Amount {
    return this.amount = amount;
  }

  int CountVet() {
    int count = amount;
    name = ".";
    return count;
  }

}

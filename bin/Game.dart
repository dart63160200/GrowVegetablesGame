import 'dart:io';
import 'dart:mirrors';

import 'Garden.dart';
import 'Object.dart';
import 'package:chalkdart/chalk.dart';
class Game {
  late Garden garden;
  late Me me;
  late int pay = 0;
  late int countVegALL = 0;
  late int countCarrot = 0;
  late int countOnion = 0;
  late int countTomato = 0;
  Game() {}

  void play() {
    print(chalk.redBright("Please input width and height Garden"));
    int width = int.parse(stdin.readLineSync()!);
    int height = int.parse(stdin.readLineSync()!);
    Garden garden = new Garden(width, height);
    garden.ShowGarden();
    print("----------------");

    print(chalk.greenBright("Input row and column ME "));
    var rowME = int.parse(stdin.readLineSync()!);
    var colMe = int.parse(stdin.readLineSync()!);
    Me me = new Me(chalk.greenBright("M"),colMe,rowME, 50, garden);
    print(chalk.yellow(chalk.yellow("You have Money : " + me.getMoney.toString())));
    garden.setMe(me);

    print("----------------");

    while (true) {
      garden.ShowGarden();
      print("----------------");
      inputDirectionMe();
      var dir = stdin.readLineSync()!;
      if (dir == 'q') {
        print("ํYou Quit Game");
        break;
      }
      me.walk(dir); // Me เดิน
      
      garden.ShowGarden(); //เเสดงเเปลกผัก
      print("----------------");
      ChoosePlant(); //เลือกผักที่จะปลูก

      var vegetable = stdin.readLineSync()!;
      switch (vegetable) {
        case '1':
          print(chalk.rgb(238,130,238)("Input row and column Plant Vegetable "));
          var colVe = int.parse(stdin.readLineSync()!); // รับ col ผัก 
          var rowVe = int.parse(stdin.readLineSync()!);//รับ row ผัก
         
          Vegetable veg = new Vegetable((chalk.rgb(255, 140, 0)("C")), rowVe, colVe, 10);
         // BuyVegetable(me, veg); // เช็คว่าซื้อผักได้ไหม
          if(BuyVegetable(me, veg)== false){
            break;
          }
          ChooseCarrot(veg);//เลือกซื้อเเครอท
          garden.setVegetable(veg);//ใส่เเครอทในตาราง

          break;
        case '2':
          print(chalk.rgb(238,130,238)("Input row and column Plant Vegetable "));
          var colVe = int.parse(stdin.readLineSync()!);
          var rowVe = int.parse(stdin.readLineSync()!);
          
          Vegetable veg = new Vegetable("O",rowVe,colVe, 15);
         // BuyVegetable(me, veg);
          if(BuyVegetable(me, veg)== false){
            break;
          }
          ChooseOnion(veg);
          garden.setVegetable(veg);
          break;

        case '3':
          print(chalk.rgb(238,130,238)("Input row and column Plant Vegetable "));
           var colVe = int.parse(stdin.readLineSync()!);
           var rowVe = int.parse(stdin.readLineSync()!);
         
          Vegetable veg = new Vegetable((chalk.rgb(255,0, 0)("T")),rowVe ,colVe, 20);
          //BuyVegetable(me, veg);
          if(BuyVegetable(me, veg)== false){
            break;
          }
          ChooseTomato(veg);
          garden.setVegetable(veg);
          break;

        default:
          break;
      }
      print("----------------");
    }
  }



  bool BuyVegetable(Me me, Vegetable veg) {
    if (me.getMoney >= veg.getPrice) {
      pay = me.getMoney - veg.getPrice;
      print("----------------");
      print(chalk.yellow("You have Money : " + pay.toString()));
      me.setMoney(pay);
      return true;
    } else {
      print(chalk.redBright("You Not Meney !!!"));
      return false;
    }
  }

  void ChoosePlant() {
    // เลือกผัก
    print("You choose Plant Vegetable");
    print("1.Carrot price 10 Bath");
    print("2.Onion price 15 Bath");
    print("3.Tomato price 20 Bath");
  }

  void inputDirectionMe() {
    // เลือกทิศทาง
    print("You input direction :");
    print(chalk.cyanBright("intput N or n is North"));
    print(chalk.cyanBright("intput W or w is West"));
    print(chalk.cyanBright("intput E or e is East"));
    print(chalk.cyanBright("intput S or s is South"));
    print(chalk.redBright("intput q is Quit Game"));
    
  }

  void ChooseCarrot(Vegetable veg) {
    // เลือก 1 Carrot
    print("You choose Carrot Plant " +
        "Name : " +
        " " +
        veg.getName +
        ", " +
        "Row : " +
        veg.getCol.toString() +
        " ," +
        "Column : " +
        veg.getRow.toString() +
        " ,"
            "Price: " +
        veg.getPrice.toString() +
        " Bath");
  }

  void ChooseOnion(Vegetable veg) {
    // เลือก 2 Onion
    print("You choose Onion Plant " +
        "Name : " +
        " " +
        veg.getName +
        ", " +
        "Row : " +
        veg.getCol.toString() +
        " ," +
        "Column : " +
        veg.getRow.toString() +
        " ,"
            "Price: " +
        veg.getPrice.toString() +
        " Bath");
  }

  void ChooseTomato(Vegetable veg) {
    // เลือก 3 Onion
    print("You choose Tomato Plant " +
        "Name : " +
        " " +
        veg.getName +
        ", " +
        "Row : " +
        veg.getCol.toString() +
        " ," +
        "Column : " +
        veg.getRow.toString() +
        " ,"
            "Price: " +
        veg.getPrice.toString() +
        " Bath");
  }
}

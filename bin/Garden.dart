import 'dart:core';
import 'dart:ffi';
import 'dart:io';
import 'package:chalkdart/chalk.dart';
import 'package:test/test.dart';

import 'Object.dart';

class Garden {
  late int width;
  late int height;
  late Me me;
  late Vegetable vegetable;

  List<Obj> Objects = [];
  int objCount = 0;

  Garden(int width, int height) {
    // constructor
    this.width = width;
    this.height = height;
  }

  void printSymbolOn(int x, int y) {
    var symbol = chalk.rgb(139,69,19).bold(".");
    for (var i = 0; i < objCount; i++) {
      if (Objects[i].isOn(x, y)) {
        symbol = Objects[i].getName;
      }
    }
    stdout.write(symbol + "  "); //ระยะห่าง symbol
  }


  void ShowGarden() {

    for (var i = 0; i < width; i++) {
      stdout.write("  " + (i + 1).toString() + "");
    }
    print(" ");
    for (var col = 0; col < height; col++) {
      print("");
      for (var row = 0; row < height; row++) {
        if (row == 0) {
          stdout.write((col + 1).toString() + " "); // ปริ้นตัวเลขเเถว
        }

        printSymbolOn(row+1,col+1); // ปริ้น 
      }
      print(" ");
    }
  }

  void setMe(Me me) {
    this.me = me;
   /*  addObj(me); */
    Objects.add(me);/*  */
    objCount++;
  }

  void setVegetable(Vegetable vegetable){
    this.vegetable=vegetable;
    Objects.add(vegetable);
    objCount++;
  }

  void showObj(Obj obj) {
    print(obj.getName);
  }
  void PrintList(){
    print(Objects);
  }

  bool inGarden(int x,int y){ //ในสวน
    return (x >=1 && x<width)&&(y>=1 && y< height);
  }

  bool isVegetable(int x, int y){
    return vegetable.isOn(x, y);
  }
 

}
